﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour {

    public float spawnWait;
    public Vector3 spawnPos;
    public GameObject car;
    public int maxCars;
    public int activeCars;
    public int coinsCollected; 
    public int totalCoins; 
    public bool noSpawn;

    public bool gameOver;
    private bool gameWin;
    private WinLoseDisplay wlDisplay;



    

    void Start () {
        gameOver = false;
        gameWin = false;
        StartCoroutine(SpawnCars());
        wlDisplay = GameObject.FindGameObjectWithTag("WinLose").GetComponent<WinLoseDisplay>();
	}

    IEnumerator SpawnCars()
    {
        while (!gameOver && !gameWin)
        {
            yield return new WaitForSeconds(spawnWait);
            Quaternion spawnRot = Quaternion.identity;
            if (activeCars < maxCars && !noSpawn && (!gameOver && !gameWin))
            {
                Instantiate(car, spawnPos, spawnRot);
            }
        }
    }

    void Update () {
		if (gameOver == true)
        {
            wlDisplay.LoseGame();
        }

        if (coinsCollected == totalCoins)
        {
            wlDisplay.WinGame();
            gameWin = true;
            foreach (GameObject car in GameObject.FindGameObjectsWithTag("Enemy"))
            {
                Destroy(car);
            }
        }
	}
}