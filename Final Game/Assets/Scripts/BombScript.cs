﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BombScript : MonoBehaviour {

    public float radius;
    public float force;
    public GameObject explosion;

    private int i; // Debug
    private Rigidbody _rb;
    private int bouncesLeft;

	void Start () {
        _rb = GetComponent<Rigidbody>();
        bouncesLeft = 3;
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Bounceable")
        {
            bouncesLeft -= 1;
        }

        if (collision.gameObject.tag == "Enemy")
        {
            Explode();
            Debug.Log(collision.gameObject.GetComponent<CarScript>().health);
        }
    }

    void Explode ()
    {
        i = 0; // Debug
        foreach (Collider enemy in Physics.OverlapSphere(transform.position, radius))
        {
            if (enemy.gameObject.GetComponent<CarScript>() != null)
            {
                i++; // Debug
                enemy.gameObject.GetComponent<CarScript>().health -= 5;
                Debug.Log(i.ToString() + " Car Health: " + enemy.GetComponent<CarScript>().health);
            }
        }
        Instantiate(explosion, transform.position, transform.rotation);
        Destroy(gameObject);
    }

    // Update is called once per frame
    void Update () {
        if (bouncesLeft <= 0)
        {
            Explode();
        }
    }
}
