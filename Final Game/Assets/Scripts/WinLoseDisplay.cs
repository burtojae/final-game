﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WinLoseDisplay : MonoBehaviour {

    private Text displayText;
    private GameController gameController;

    public Color winColor;
    public Color loseColor;

    private void Start()
    {
        gameController = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameController>();
        displayText = GetComponent<Text>();
    }

    public void WinGame()
    {
        if (displayText.text == "")
        {
            displayText.text = "You win!";
            displayText.color = winColor;
        }
    }

    public void LoseGame()
    {
        if (displayText.text == "")
        {
            displayText.text = "You got caught!";
            displayText.color = loseColor;
        }
    }
}
