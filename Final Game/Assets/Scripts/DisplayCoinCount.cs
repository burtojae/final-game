﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DisplayCoinCount : MonoBehaviour {

	private Text displayText;
    private GameController gameController;


    private void Start()
    {
        gameController = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameController>();
        displayText = GetComponent<Text>();
    }

    void Update () {
        displayText.text = "Coins: " + gameController.coinsCollected + "/" + gameController.totalCoins;
	}
}
