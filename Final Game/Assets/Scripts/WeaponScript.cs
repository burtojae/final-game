﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponScript : MonoBehaviour {

    private float nextFire;
    private Rigidbody _rb;
    private Camera playerCam;
    private GameObject bomb;

    public float fireRate;
    public GameObject shot;
    public GameObject playerChar;
    public float force;
    public Vector3 shotOffset;


    // Use this for initialization
    void Start () {
        _rb = GetComponent<Rigidbody>();
        playerCam = playerChar.GetComponent<Camera>();
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetButton("Fire1") && Time.time > nextFire)
        {
            nextFire = Time.time + fireRate;
            bomb = Instantiate(shot, (_rb.position + _rb.transform.forward) - shotOffset, Quaternion.identity);
            bomb.GetComponent<Rigidbody>().velocity = (GameObject.FindGameObjectWithTag("TargetPoint").transform.position - transform.position) * force;
        }
    }
}
