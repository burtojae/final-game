﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class CarScript : MonoBehaviour {

    public Transform target;
    public Transform selfPos;
    public float health;
    public Color threeQuartHTint;
    public Color halfHTint;
    public Color lowHTint;

    private NavMeshAgent navComponent;
    private float dist;
    private GameController gCont;
    private Light aura;
    private bool flashOut;
    private bool flashIn;
    private Color baseTint;


    void Start () {
        target = GameObject.FindGameObjectWithTag("Player").transform;
        aura = GetComponent<Light>();
        flashOut = false;
        flashIn = false;
        selfPos = this.transform;
        baseTint = aura.color;
        navComponent = this.GetComponent<NavMeshAgent>();
        gCont = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameController>();
        gCont.activeCars += 1;
    }
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            //GameObject.FindGameObjectWithTag("Fade").GetComponent<FadeOutScript>().fade = true;
            //gCont.gameOver = true;
        }
    }
    

    void Update () {
        dist = Vector3.Distance(target.position, selfPos.position);

        if (target)
        {
            navComponent.SetDestination(new Vector3(target.position.x, 0, target.position.z));
        }
        else
        {
            if (target == null)
            {
                target = this.gameObject.GetComponent<Transform>();
            }
            else
            {
                target = GameObject.FindGameObjectWithTag("Player").transform;
            }
        }

        if (health <= 0)
        {
            gCont.activeCars -= 1;
            Destroy(gameObject);
        }
        else if (health == 5)
        {
            flashOut = true;
            aura.color = lowHTint;
        }
        else if (health == 10)
        {
            aura.color = halfHTint;
        }
        else if (health == 15)
        {
            aura.color = threeQuartHTint;
        }

        if (flashIn)
        {
            aura.intensity += 0.01f;
            if (aura.intensity >= 2)
            {
                flashIn = false;
                flashOut = true;
            }
        }

        if (flashOut)
        {
            aura.intensity -= 0.01f;
            if (aura.intensity <= 0.5)
            {
                flashOut = false;
                flashIn = true;
            }
        } //Was supposed to flash but doesn't work. Messed something up here logic-wise and don't have time to fix.
    }

}
