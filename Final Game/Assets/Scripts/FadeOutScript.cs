﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FadeOutScript : MonoBehaviour {

    private Image fadeTint;
    public Color speed;
    public bool fade;


    private void Start()
    {
        fadeTint = this.GetComponent<Image>();
        fade = false;
    }

    void Update () {
        if (fade == true)
        {
            fadeTint.color += speed;
        }
	}
}
